import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { HttpRequest } from '@angular/common/http';

import { NotificationComponent } from '../modals/notification/notification.component';

@Injectable()
export class AuthGuard implements CanActivate {
    cachedRequests: Array<HttpRequest<any>> = [];

    constructor(private router: Router, public notify: NotificationComponent) { }

    public getToken(): string {
        return localStorage.getItem('token');
    }

    public canActivate() {
        if (localStorage.getItem('token')) {
            return true;
        } else {
            this.router.navigate(['/']);
            this.notify.showError("Unauthorized Access", "You must be logged in to access this page");
            return false;
        }
    }

    public collectFailedRequest(request): void {
        this.cachedRequests.push(request);
    }

    public retryFailedRequests(): void {
        // retry the requests. this method can
        // be called after the token is refreshed
    }
}
