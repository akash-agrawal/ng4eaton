import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../../../app.global';

@Injectable()
export class ReplaceAssetModalService {

    constructor(private http: HttpClient) { }

    getAllAssets(config) {
        const body = JSON.stringify(config);
        return this.http.post(myGlobals.baseUrl + 'asset/search', body);
    }

    replaceAsset(config) {
        const body = JSON.stringify(config);
        return this.http.post(myGlobals.baseUrl + 'assetSearchReplace/replace', body);
    }
}
