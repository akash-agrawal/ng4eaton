export class ReplaceAssetModel {
    public assetList: any;
    public rows: any;
    public config: any;
    public replaceAsset: any;
    public replaceBy: string;
    public replaceAssetform: boolean;
    public selectedAsset:any;
    public otherReason: boolean;
    public replaceConfig: any;
    public searchText: any;
    public filteredList:any;
    public selected:any;
    public replaceForm:boolean;
    public formatedDate:any;
    public createdDate:any;
    public monthIndex:any;
    public day:any;
    public year:any;
    public myFormattedDate:any;
    public datePipe:any;
    
    constructor() {
        this.rows = [];
        this.config = [];
        this.assetList = [];
        this.replaceAsset = [];
        this.replaceBy = "Eaton Corporation";
        this.replaceAssetform = false;
        this.selectedAsset = [];
        this.otherReason = false;
        this.replaceConfig = [];
        this.searchText = "";
        this.filteredList = [];
        this.selected = [];
        this.replaceForm = true;
        this.createdDate = [];
        this.formatedDate = "";
        this.monthIndex = "";
        this.day = "";
        this.year = "";
        this.myFormattedDate = "";
        this.datePipe = "";
        
        
    }
}
