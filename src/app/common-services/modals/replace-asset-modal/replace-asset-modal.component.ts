import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ReplaceAssetModalService } from './replace-asset-modal.service';
import { ReplaceAssetModel } from './replace-asset-modal';
import { NotificationComponent } from '../notification/notification.component';

@Component({
    selector: 'app-replace-asset-modal',
    templateUrl: './replace-asset-modal.component.html',
    styleUrls: ['./replace-asset-modal.component.css'],
    providers: [ReplaceAssetModalService]
})
export class ReplaceAssetModalComponent {
    @ViewChild('replaceAssetModal') public replaceAssetModal: ModalDirective;
    private model: ReplaceAssetModel;
   
    constructor(private replaceAssetModalService: ReplaceAssetModalService, public notify: NotificationComponent, private router: Router) {
        this.model = new ReplaceAssetModel();
    }

    onSubmit(form: NgForm) {
        //console.log("form submitted", form.value.replacementReason);
        if(form.value.replacementReason == 'Other'){
         form.value.replacementReason = form.value.otherReason;
        }else{
         form.value.replacementReason = form.value.replacementReason;
        }
        this.model.replaceConfig = {
            "currentasset": {
                "assetId": (this.model.selectedAsset as any)._id
            },
            "replacementType": form.value.replacementType,
            "replacementReason": form.value.replacementReason
        }
        //console.log("config", this.model.replaceConfig);
        this.replaceAsset(this.model.replaceConfig);
        
       
    }

    updateFilter(filterValue) {
        const lowerValue = filterValue.toLowerCase();
        this.model.filteredList = this.model.rows.filter(item => item.assetNumber.toLowerCase().indexOf(lowerValue) !== -1 || !lowerValue);
    }

    show() {
        this.replaceAssetModal.show();
        this.model.config = {
            "initialLoad": true,
            "order": "-createdDate",
            "pageNo": 1,
            "pageSize": 10,
            "searchDefaultLocation": true
        }
        this.getAssetList(this.model.config);

        // this.getAssetList();
    }

    clearSearch() {
        this.model.searchText = "";
        this.updateFilter("");
    }

    replaceAsset(replaceConfig) {
        this.replaceAssetModalService.replaceAsset(replaceConfig)
            .subscribe(
            data => {
                this.model.replaceAsset = data;
                console.log("this.model.replaceAsset", this.model.replaceAsset);
            },
            error => {
                console.log(error);
            },
            () =>{
                this.notify.showSuccess("Success", "Asset replaced successfully");
                this.router.navigate(['/asset']);
            }
        );


    }

    hide(form: NgForm) {
        form.resetForm();
        this.replaceAssetModal.hide();
    }
    onSelect({ selected }) {
        console.log('Select Event', selected);
        this.model.selectedAsset = selected[0];
        this.model.replaceAssetform = true;
        this.model.replaceForm = false;
        
    }
    otherSelect(event) {
        if (event.target.value == 'Other') {
            this.model.otherReason = true;
        } else {
            this.model.otherReason = false;
        }

    }

    onActivate(event) {
        //console.log('Activate Event', event);
    }
    getAssetList(config) {
        this.replaceAssetModalService.getAllAssets(config)
            .subscribe(
            data => {
                this.model.assetList = data;
                console.log("this.model.assetList", this.model.assetList);
                this.model.rows = this.model.assetList.assetList;
                this.model.filteredList = this.model.rows;
                
            },
            error => {
                console.log(error);
            });
    }


}
