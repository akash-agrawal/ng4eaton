import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import * as myGlobals from '../../../app.global';

@Injectable()
export class CloneAssetModalService {

  constructor(private http: HttpClient) { }

  getAllAssets(config) {
    const body = JSON.stringify(config);
    return this.http.post(myGlobals.baseUrl + 'asset/search', body);
  }

  cloneAssets(config) {
    const body = JSON.stringify(config);
    return this.http.post(myGlobals.baseUrl + 'asset/clone', body);
  }

  getOwners(tenantId) {
    return this.http.get(myGlobals.baseUrl + 'customer/owners/lookup/' + tenantId);
  }

  getLocations(tenantId) {
    return this.http.get(myGlobals.baseUrl + 'location/list/' + tenantId);
  }

  getAllMarketSegments(tenantId) {

    return this.http.post(myGlobals.baseUrl + 'getAllMarketSegments', tenantId);

  }

  getApplication(tenantId) {

    return this.http.get(myGlobals.baseUrl + 'getCustomerMarketSegmentApplication/tenant%3A%3A200002');

  }

  getMaintenanceSchedule(tenantId){
    
    return this.http.get(myGlobals.baseUrl + 'category/defaultForms/list?categoryId=category%3A%3A16a86410-c2ee-4529-b2cc-043854f42e44&tenantId='+tenantId+'&ownerId=');
  }

}
