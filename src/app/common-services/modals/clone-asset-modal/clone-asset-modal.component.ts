import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CloneAssetModalService } from './clone-asset-modal.service';
import { CloneAssetModel } from './clone-asset-modal';
import { TabsetComponent } from 'ngx-bootstrap';
import { NotificationComponent } from '../../modals/notification/notification.component';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-clone-asset-modal',
    templateUrl: './clone-asset-modal.component.html',
    styleUrls: ['./clone-asset-modal.component.css'],
    providers: [CloneAssetModalService]
})
export class CloneAssetModalComponent implements OnInit {
    @ViewChild('cloneAssetModal') public cloneAssetModal: ModalDirective;
    @ViewChild('staticTabs') staticTabs: TabsetComponent;
    private model: CloneAssetModel;
    private assetModel:CloneAssetModel;
    public rows = [];
    public filteredList = [];
    public columns;
    public quan = 1;
    private searchText = "";
    public btnDisable = true;
    public status: any = {
        isFirstOpen: true
    };
    private tenantDetails: any;
    materials: any = ['Overall Hose Length', 'Fitting 1 Part Number', 'Raw Hose Cut Length', 'Fitting 2 Part Number', 'Raw Hose Part Number'];
    customerSpecific: any = ['Customer Part Number', 'Customer Specific on Label Notes', 'Market Segment', 'Customer Revision Number', 'Customer Name', 'Application', 'Customer Special Notes', 'Customer Location'];
    dimensionDetails: any = ['Bend Radius', 'Hose ID', 'Pressure Rating', 'Warranty Information', 'Application', 'Crimp Diameter', 'Hose OD', 'Impulse Cycle Life', 'Phase Angle', 'Hose series', 'Drop Dimension', 'Abrasion Resistance', 'Market Segment'];

    constructor(private cloneAssetModalService: CloneAssetModalService, public notify: NotificationComponent) {
        this.model = new CloneAssetModel();
        this.assetModel=new CloneAssetModel();
    }

    ngOnInit() {

    }
    updateFilter(filterValue) {
        const lowerValue = filterValue.toLowerCase();
        this.filteredList = this.rows.filter(item => item.assetNumber.toLowerCase().indexOf(lowerValue) !== -1 || !lowerValue);
    }

    getAssetList(config) {
        this.cloneAssetModalService.getAllAssets(config)
            .subscribe(
            data => {
                this.model.assetList = data;
                //console.log("this.model.assetList", this.model.assetList);
                this.rows = this.model.assetList.assetList;
                this.filteredList = this.rows;
            },
            error => {
                //console.log(error);
            });
    }

    show() {
        this.LoadData();
        this.cloneAssetModal.show();
    }

    hide(form: NgForm) {
        form.resetForm();
        this.cloneAssetModal.hide();

    }

    LoadData() {
        this.tenantDetails = JSON.parse(localStorage.getItem('profile'));
        this.selectTab(0);
        this.btnDisable = true;
        this.model.autoHeight = window.innerHeight;
        this.model.config = {
            "initialLoad": true,
            "order": "-createdDate",
            "pageNo": 1,
            "pageSize": 10,
            "searchDefaultLocation": true
        }
        this.getAssetList(this.model.config);
    }

    onActivate(event) { }


    onSelect(event) {
        this.btnDisable = false;
        this.model.newMaterials=[];       
        this.model.newCustomerSpecific=[]; 
        this.model.newDimensionDetails=[];
        this.model.assetData = event.selected[event.selected.length - 1];
        console.log(this.model.assetData);
        this.getOwners(this.tenantDetails.tenantId);
        this.getLocations(this.tenantDetails.tenantId);
        this.getAllMarketSegments(this.tenantDetails.tenantId);
        this.getApplication(this.tenantDetails.tenantId);
        this.getMaintenanceSchedule(this.tenantDetails.tenantId);

        this.model.accessoriesData = this.model.assetData.accessories;
        let i: number = 0;
        while (i < this.model.assetData.attributes.length) {
            for (let toRem of this.materials) {
                if (toRem === this.model.assetData.attributes[i].attributeName) {
                    this.model.newMaterials.push(this.model.assetData.attributes[i]); // remove duplicated
                    continue;
                }
            }
            ++i;
        }

        let j: number = 0;
        while (j < this.model.assetData.attributes.length) {
            for (let toRem of this.customerSpecific) {
                if (toRem === this.model.assetData.attributes[j].attributeName) {
                    this.model.newCustomerSpecific.push(this.model.assetData.attributes[j]); // remove duplicated
                    continue;
                }
            }
            ++j;
        }

        let k: number = 0;
        while (k < this.model.assetData.attributes.length) {
            for (let toRem of this.dimensionDetails) {
                if (toRem === this.model.assetData.attributes[k].attributeName) {
                    this.model. newDimensionDetails.push(this.model.assetData.attributes[k]); // remove duplicated
                    continue;
                }
            }
            ++k;
        }

        //console.log(this.model.assetData.manufactureLocation.name);
    }

    modifyAndClone() {
        this.selectTab(1);
    }

    backToAssetList() {
        this.selectTab(0);
    }

    selectTab(tab_id: number) {
        this.staticTabs.tabs[tab_id].active = true;
    }

    clearSearch() {
        this.searchText = "";
        this.updateFilter("");
    }

    cloneAsset(form: NgForm) {
        var postData = {
            "_id": this.model.assetData._id,
            "quantity": this.quan,
        }
        this.btnDisable = true;
        this.cloneAssetModalService.cloneAssets(postData)
            .subscribe(
            data => {
                this.btnDisable = false;
                this.model.assetList = data;
                //console.log("this.model.assetList", this.model.assetList);  

            },
            error => {
                //console.log(error);
                this.notify.showError("Success", error);
            }, () => {
                form.resetForm();
                this.cloneAssetModal.hide();
                this.notify.showSuccess("Success", "Colne Asset created successfully");
            });
    }

    modifyAndCloneAsset(form: NgForm) {
        //console.log("form submitted", form);
        form.resetForm();
    }

    getOwners(tenantId) {
        this.cloneAssetModalService.getOwners(tenantId).subscribe(
            data => {  
                //console.log('getOwners',data);              
                this.model.ownerData = data;
            },
            error => {
                //console.log(error);
            },
            () => {

            });

    }

    getLocations(tenantId) {

        this.cloneAssetModalService.getLocations(tenantId).subscribe(
            data => {
                //console.log('getLocations',data);         
                this.model.locationsData = data;
            },
            error => {
                //console.log(error);
            },
            () => {
            });
    }

    getAllMarketSegments(tenantId) {
        this.cloneAssetModalService.getAllMarketSegments(tenantId).subscribe(
            data => {
                //console.log('getAllMarketSegments',data);      
                this.model.marketSegmentData = data;
            },
            error => {
                console.log(error);
            },
            () => {

            });
    }

    getApplication(tenantId) {
        this.cloneAssetModalService.getApplication(tenantId).subscribe(
            data => {
                //console.log('getApplication',data);      
                this.model.applicationData = data;
            },
            error => {
                console.log(error);
            },
            () => {

            });
    }

    getMaintenanceSchedule(tenantId) {
        this.cloneAssetModalService.getMaintenanceSchedule(tenantId).subscribe(
            data => {
                //console.log('getMaintenanceSchedule',data); 
                this.model.maintenanceData = data;
            },
            error => {
                console.log(error);
            },
            () => {

            });

    }

    modifyCloneAsset(form: NgForm) {     
        this.model.assetData.attributes=[];        
        this.model.newMaterials.forEach(element => {
            this.model.assetData.attributes.push(element);
        });
        this.model.newCustomerSpecific.forEach(element => {
            this.model.assetData.attributes.push(element);
        });
        this.model.newDimensionDetails.forEach(element => {
            this.model.assetData.attributes.push(element);
        });
        this.model.assetData.quantity=this.quan;
        this.model.assetData.isModified = true;

        //console.log(form);
        //console.log(this.model.assetData);

        this.cloneAssetModalService.cloneAssets(this.model.assetData)
        .subscribe(
        data => {
            this.btnDisable = false;
            this.model.assetList = data;
        },
        error => {
            this.notify.showError("Success", error);
        }, () => {
            form.resetForm();
            this.cloneAssetModal.hide();
            this.notify.showSuccess("Success", "Clone Asset created successfully");
        });
    }

}
