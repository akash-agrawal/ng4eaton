import { Component, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-confirm-action-modal',
    templateUrl: './confirm-action-modal.component.html',
    styleUrls: ['./confirm-action-modal.component.css']
})
export class ConfirmActionModalComponent {
    @ViewChild('confirmActionModal') public confirmActionModal: ModalDirective;

    public message: string = "test";
    public actionReturn: boolean = null;

    constructor() { }

    

}
