import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { AddLocationModalService } from './add-location-modal.service'

@Component({
    selector: 'app-add-location-modal',
    templateUrl: './add-location-modal.component.html',
    styleUrls: ['./add-location-modal.component.css'],
    providers: [AddLocationModalService]
})
export class AddLocationModalComponent implements OnInit {
    @ViewChild('addLocationModal') public addLocationModal: ModalDirective;
    public chipIdValidate: boolean = false;
    public parentId: any;
    public tenantId: any;

    constructor(private addLocationModalService: AddLocationModalService) { }

    ngOnInit() {
    }

    show(parentId, tenantId) {
        this.parentId = parentId;
        this.tenantId = tenantId;
        this.addLocationModal.show();
    }

    hide(form: NgForm) {
        form.resetForm();
        this.addLocationModal.hide();
    }

    createNewLocation(form: NgForm) {
        console.log("form submitted", form);
        this.addLocationModalService.createLocation(form, this.parentId, this.tenantId)
            .subscribe(
            data => {
                console.log(data);
            },
            error => {
                console.log(error);
            },
            () => {
                form.resetForm();
                this.addLocationModal.hide();
            });
    }

    chipIdCheck($event) {
        console.log($event.target.value.length);
        if ($event.target.value.length == 16) {
            this.chipIdValidate = false;
        } else if ($event.target.value.length == 24) {
            this.chipIdValidate = false;
        } else if ($event.target.value.length == 32) {
            this.chipIdValidate = false;
        } else {
            this.chipIdValidate = true;
        }

    }

}
