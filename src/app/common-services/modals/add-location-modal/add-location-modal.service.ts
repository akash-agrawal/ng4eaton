import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../../../app.global';

@Injectable()
export class AddLocationModalService {

    constructor(private http: HttpClient) { }

    createLocation(formData, parentId, tenantId) {
        const obj = {
            "name": formData.value.locationName,
            "parentId": parentId,
            "description": formData.value.desc,
            "notes": formData.value.notes,
            "customerLocationID": formData.value.custID,
            "locGPSLat": formData.value.locLat,
            "locGPSLong": formData.value.locLong,
            "chipIds": [
                formData.value.chipID
            ],
            "barcode": formData.value.barcode,
            "tenantId": tenantId,
            "isSync": false
        }
        const body = JSON.stringify(obj);
        return this.http.post(myGlobals.baseUrl + 'location/save/tree', body);
    }

}
