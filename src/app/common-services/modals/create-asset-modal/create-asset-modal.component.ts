import { Component, ViewChild, Input, Output } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TabsetComponent } from 'ngx-bootstrap';

import { CreateAssetModalService } from './create-asset-modal.service';
import { CreateAssetModel } from './create-asset-modal';

import { LocationTreeViewComponent } from '../location-tree-view/location-tree-view.component';

@Component({
    selector: 'app-create-asset-modal',
    templateUrl: './create-asset-modal.component.html',
    styleUrls: ['./create-asset-modal.component.css'],
    providers: [CreateAssetModalService]
})
export class CreateAssetModalComponent {
    @ViewChild('createAssetModal') public createAssetModal: ModalDirective;
    @ViewChild('locationTreeViewModal') public locationTreeViewModal: LocationTreeViewComponent;
    @ViewChild('staticTabs') staticTabs: TabsetComponent;

    private model: CreateAssetModel;

    constructor(private createAssetModalService: CreateAssetModalService) {
        this.model = new CreateAssetModel();
    }

    //get location from the "Location Tree View Modal"
    selectedLocObj($event) {
        this.model.selectedLocation = $event;
        console.log("success", this.model.selectedLocation);
        this.model.selectedLocationName = $event.name;
    }

    selectTab(tab_id: number) {
        this.staticTabs.tabs[tab_id].active = true;
    }

    show() {
        this.createAssetModal.show();
        this.getQuantity();
        this.getCustomers();
    }

    hide() {
        this.createAssetModal.hide();
    }

    showLocationTreeView() {
        this.locationTreeViewModal.show();
    }

    getQuantity() {
        this.model.quantityList = Array.from(Array(100).keys());
    }

    getCustomers() {
        const tenantId = JSON.parse(localStorage.getItem('profile'));
        this.createAssetModalService.getCustomers(tenantId.tenantId)
            .subscribe(
            data => {
                this.model.customerList = data;
            },
            error => {
                console.log(error);
            });
    }

    getLocations(tenantId) {
        this.createAssetModalService.getLocations(tenantId)
            .subscribe(
            data => {
                this.model.locationList = data;
            },
            error => {
                console.log(error);
            });
    }

    getMarketSegments(tenantId) {
        this.createAssetModalService.getMarketSegments(tenantId)
            .subscribe(
            data => {
                this.model.marketSegment = data;
            },
            error => {
                console.log(error);
            });
    }

    customerChange(tenantId) {
        this.getLocations(tenantId);
        this.getMarketSegments(tenantId);
    }

    selectHose() {
        this.createAssetModalService.getComponentPart('Raw Hose', true)
            .subscribe(
            data => {
                this.model.hoseList = data;
                console.log("this.model.hoseList", this.model.hoseList);
            },
            error => {
                console.log(error);
            },
            () => {
                this.selectTab(1);
            });
    }

    getPartDetail(hose) {
        console.log("hose", hose);
        let hoseList = this.model.hoseList.componentPartList;
        for (let key in hoseList) {
            this.model.selectedHose = hoseList.filter(obj => obj._id == hose).map(obj => obj.componentPartNumber)[0];
        }
        console.log("selectedHose", this.model.selectedHose);
        this.createAssetModalService.getComponentPartbyId(hose)
            .subscribe(
            data => {
                this.model.componentPartbyIdList = data;
            },
            error => {
                console.log(error);
            },
            () => {
                //get component part id for list to be used in the fittings tab
                this.getFittings(hose);
                //Get component part (hose) image after getting hose list
                this.createAssetModalService.getComponentImage(this.model.componentPartbyIdList)
                    .subscribe(
                    data => {
                        this.model.componentPartImage = data;
                    },
                    error => {
                        console.log(error);
                    });
            }
            );
    }

    getFittings(hose) {
        this.createAssetModalService.getComponentPartIdForList(hose)
            .subscribe(
            data => {
                this.model.componentPartForList = data;
                console.log("fittings list", data);
                if (this.model.componentPartForList.length == 0) {
                    this.model.showErrorMessage = true;
                } else {
                    this.model.showErrorMessage = false;
                }
            },
            error => {
                console.log(error);
            });
    }

    getFitting1Detail(id) {
        let fitting1List = this.model.componentPartForList;
        for (let key in fitting1List) {
            this.model.selectedFitting1 = fitting1List.filter(obj => obj._id == id).map(obj => obj.componentPartNumber)[0];
        }
        this.model.selectedFitting1Id = id;
        console.log("selectedFitting1", this.model.selectedFitting1);
        this.createAssetModalService.getComponentPartbyId(id)
            .subscribe(
            data => {
                this.model.fitting1Detail = data;
                console.log("this.model.fitting1Detail", this.model.fitting1Detail);
            },
            error => {
                console.log(error);
            },
            () => {
                //Get component part (hose) image after getting hose list
                this.createAssetModalService.getComponentImage(this.model.fitting1Detail)
                    .subscribe(
                    data => {
                        this.model.fitting1Image = data;
                    },
                    error => {
                        console.log(error);
                    });
            });
    }

    getFitting2Detail(id) {
        let fitting2List = this.model.componentPartForList;
        for (let key in fitting2List) {
            this.model.selectedFitting2 = fitting2List.filter(obj => obj._id == id).map(obj => obj.componentPartNumber)[0];
        }
        this.model.selectedFitting2Id = id;
        console.log("selectedFitting2", this.model.selectedFitting2);
        this.createAssetModalService.getComponentPartbyId(id)
            .subscribe(
            data => {
                this.model.fitting2Detail = data;
                console.log("this.model.fitting2Detail", this.model.fitting2Detail);
            },
            error => {
                console.log(error);
            },
            () => {
                //Get component part (hose) image after getting hose list
                this.createAssetModalService.getComponentImage(this.model.fitting2Detail)
                    .subscribe(
                    data => {
                        this.model.fitting2Image = data;
                    },
                    error => {
                        console.log(error);
                    });
            });
    }
}
