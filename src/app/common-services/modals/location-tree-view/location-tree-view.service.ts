import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../../../app.global';

@Injectable()
export class LocationTreeViewService {

    constructor(private http: HttpClient) { }

    getLocation(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'location/tree/list/' + tenantId);
    }

}
