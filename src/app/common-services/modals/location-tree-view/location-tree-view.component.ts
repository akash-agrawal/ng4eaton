import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { LocationTreeViewService } from './location-tree-view.service';
import { LocationTreeViewModel } from './location-tree-view';

@Component({
    selector: 'app-location-tree-view',
    templateUrl: './location-tree-view.component.html',
    styleUrls: ['./location-tree-view.component.css'],
    providers: [LocationTreeViewService]
})
export class LocationTreeViewComponent implements OnInit {
    @ViewChild('locationTreeViewModal') public locationTreeViewModal: ModalDirective;
    @Output() sendLocObj = new EventEmitter<any>();
    
    private model: LocationTreeViewModel;

    constructor(private locationTreeViewService: LocationTreeViewService) {
        this.model = new LocationTreeViewModel();
    }

    ngOnInit() {
    }

    show() {
        this.locationTreeViewModal.show();
        this.getLocation();
    }

    hide() {
        this.locationTreeViewModal.hide();
    }

    getLocation() {
        const tenantId = JSON.parse(localStorage.getItem('profile')).tenantId;

        this.locationTreeViewService.getLocation(tenantId)
            .subscribe(
            data => {
                console.log("data", data);
                this.model.locationList = data;
            },
            error => {
                console.log(error);
            }
            );
    }

    selectLocEdit(id) {
        const locationList = this.model.locationList;
        this.model.locObj = locationList.filter(x => x._id == id).map(x => x)[0];
        console.log("selected location object", this.model.locObj);
        //send location details to the "Create Asset Modal"
        this.sendLocObj.emit(this.model.locObj);
        this.locationTreeViewModal.hide();
    }

    selectChildLocEdit(id) {
        const locationList = this.model.locationList;
        this.model.locObj = locationList.filter(x => x.subLocations.length !== 0).map(x => { let childArray = Object.assign({}, x); return childArray.subLocations.filter(y => y._id == id).map(y => y)[0] })[0];
        console.log("selected location object", this.model.locObj);
        //send location details to the "Create Asset Modal"
        this.sendLocObj.emit(this.model.locObj);
        this.locationTreeViewModal.hide();
    }
}
