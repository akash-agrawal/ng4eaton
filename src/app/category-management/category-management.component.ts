import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { CategoryManagementService } from './category-management.service';
import { CategoryManagementModel } from './category-management';
import { NotificationComponent } from '../common-services/modals/notification/notification.component';

@Component({
    selector: 'app-category-management',
    templateUrl: './category-management.component.html',
    styleUrls: ['./category-management.component.css'],
    providers: [CategoryManagementService]
})
export class CategoryManagementComponent implements OnInit {
    @ViewChild('addAttributesModal') public addAttributesModal: ModalDirective;
    @ViewChild('addModelModal') public addModelModal: ModalDirective;

    private model: CategoryManagementModel;

    attcolumns = [
        { name: 'Attribute Name' },
        { name: 'Attribute Type' },
        { name: 'DropDown Values' }
    ];
    defautlFormscolumns = [
        { name: 'form.name' },
        { name: 'interval' },
        { name: 'units' },
        { name: 'customerName' }
    ]

    constructor(private categoryManagementService: CategoryManagementService, public notify: NotificationComponent) {
        this.model = new CategoryManagementModel();
    }

    ngOnInit() {
        const tenantId = JSON.parse(localStorage.getItem('profile'));
        this.getManage(tenantId.tenantId);
    }

    getManage(tenantId) {
        console.log(tenantId);
        this.categoryManagementService.getManage(tenantId)
            .subscribe(
            data => {
                this.model.manageData = data;
                console.log('this.model.manageData', this.model.manageData);
            },
            error => {
                console.log(error);
            },
            () => {
                this.labelSelect(this.model.manageData.labelCategories[0]);
            });
    }

    labelSelect(labelObj) {
        this.model.labelId = labelObj._id;
        this.model.labelName = labelObj.labelName;
        this.model.categoryList = labelObj.categories;
    }

    getCategoryDetails(catObj) {
        const categoryObj: any = catObj;
        this.model.catId = categoryObj.categoryId;
        this.model.catName = categoryObj.categoryName;
        this.model.catAlias = categoryObj.categoryAlias;
        this.model.catDescription = categoryObj.categoryDescription;
        this.categoryManagementService.getCatDetail(categoryObj.categoryId)
            .subscribe(
            data => {
                //this.model.manageData = data;
                this.model.catAttributeDropDown = (data as any).categoryAttributeDropDowns;
            },
            error => {
                console.log(error);
            },
            () => {
                const tenantId = JSON.parse(localStorage.getItem('profile')).tenantId;
                this.getModelPart(categoryObj.categoryId, tenantId);
                //this.labelSelect(this.model.manageData.labelCategories[0]);
            });
    }

    getModelPart(catId, tenantId) {
        this.categoryManagementService.getModelPart(catId, tenantId)
            .subscribe(
            data => {
                this.model.modelPartDetail = data;
            },
            error => {
                console.log(error);
            },
            () => {
                //get default forms tab data after receiving model parts data
                this.getDefaultForms(catId, tenantId);
            });
    }

    getDefaultForms(catId, tenantId) {
        this.categoryManagementService.getDefaultForms(catId, tenantId)
            .subscribe(
            data => {
                this.model.defaultFormsDetail = data;
            },
            error => {
                console.log(error);
            },
            () => {

            });
    }

    updateCatDetails() {
        console.log("values", this.model.catName, this.model.catAlias, this.model.catDescription, this.model.catId, this.model.labelId);
        const categories = [{
            "categoryAlias": this.model.catAlias,
            "categoryDescription": this.model.catDescription,
            "categoryId": this.model.catId,
            "categoryName": this.model.catName,
            "defaultForms": null,
            "icoSourceCat": null,
            "legacyDontUse": false
        }]
        console.log(categories);
        this.categoryManagementService.updateCatDetails(categories, this.model.labelId)
            .subscribe(
            error => {
                console.log(error);
            },
            () => {
                this.notify.showSuccess("Success", "Details updated successfully");
            });
    }

    addAttributes() {
        this.addAttributesModal.show();
        this.categoryManagementService.getSystemAttributes(JSON.parse(localStorage.getItem('profile')).tenantId)
            .subscribe(
            data => {
                this.model.systemAttributesList = data;
                console.log("data", data);
            },
            error => {
                console.log(error);
            },
            () => {
                this.notify.showSuccess("Success", "Details updated successfully");
            });
    }

    attSelected(selectedAtt) {
        this.model.selectedAtt = selectedAtt;
    }

    addModel() {
        this.addModelModal.show();
    }
}
