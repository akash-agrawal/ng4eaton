import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../app.global';

@Injectable()
export class CategoryManagementService {

    constructor(private http: HttpClient) { }

    getManage(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'category/manage/' + tenantId);
    }

    getCatDetail(catId) {
        return this.http.get(myGlobals.baseUrl + 'category/catAttributeDropDown/' + catId);
    }

    getModelPart(catId, tenantId) {
        return this.http.get(myGlobals.baseUrl + 'model/' + catId + '/modelPartTree/' + tenantId);
    }

    getDefaultForms(catId, tenantId) {
        return this.http.get(myGlobals.baseUrl + 'category/defaultForms/list?categoryId=' + catId + '&tenantId=' + tenantId + '&ownerId=');
    }

    getSystemAttributes(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'systemAttributes/list/' + tenantId);
    }

    updateCatDetails(categories, labelId) {
        let data = {
            'categories': categories,
            '_id': labelId
        }
        const body = JSON.stringify(data);
        return this.http.post(myGlobals.baseUrl + 'label/category/save', body);
    }


}
