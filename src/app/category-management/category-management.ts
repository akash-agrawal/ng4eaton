export class CategoryManagementModel {
    public manageData: any;
    public categoryList: any;
    public labelName: string;
    public catName: string;
    public catAlias: string;
    public catDescription: string;
    public catAttributeDropDown: any;
    public modelPartDetail: any;
    public defaultFormsDetail: any;
    public catId: any;
    public labelId: any;
    public systemAttributesList: any;
    public selectedAtt: any;

    constructor() {
        this.manageData = [];
        this.categoryList = [];
        this.labelName = null;
        this.catName = null;
        this.catAlias = null;
        this.catDescription = null;
        this.catAttributeDropDown = [];
        this.modelPartDetail = [];
        this.defaultFormsDetail = [];
        this.systemAttributesList = [];
        this.selectedAtt = null;
    }
}

