import { Component, OnInit, ViewChild } from '@angular/core';

import { MyLocationService } from './my-location.service';
import { MyLocationModel } from './my-location';
import { AddLocationModalComponent } from '../common-services/modals/add-location-modal/add-location-modal.component';
import { ConfirmActionModalComponent } from '../common-services/modals/confirm-action-modal/confirm-action-modal.component';

@Component({
    selector: 'app-my-location',
    templateUrl: './my-location.component.html',
    styleUrls: ['./my-location.component.css'],
    providers: [MyLocationService]
})
export class MyLocationComponent implements OnInit {
    //creating reference of the child component to the parent component
    @ViewChild('addLocationModal') public addLocationModal: AddLocationModalComponent;
    @ViewChild('confirmActionModal') public confirmActionModal: ConfirmActionModalComponent;

    //initializing model
    private model: MyLocationModel;

    constructor(private myLocationService: MyLocationService) {
        this.model = new MyLocationModel();
    }

    ngOnInit() {
        const tenantId = JSON.parse(localStorage.getItem('profile'));
        this.getLocation(tenantId.tenantId);
    }

    //function to show "Add Location modal"
    addLocation(parentId, tenantId) {
        this.addLocationModal.show(parentId, tenantId);
    }

    //calling service to get locations
    getLocation(tenantId) {
        this.myLocationService.getLocation(tenantId)
            .subscribe(
            data => {
                console.log("data", data);
                this.model.locationList = data;
            },
            error => {
                console.log(error);
            },
            () => {
                //pre-select the first location to show the details
                this.selectLocEdit(this.model.locationList[0]._id);
            }
            );
    }

    //function to show details of selected location
    selectLocEdit(id) {
        const locationList = this.model.locationList;
        this.model.locObj = locationList.filter(x => x._id == id).map(x => x)[0];
    }

    //function to show details of selected location
    selectChildLocEdit(id) {
        const locationList = this.model.locationList;
        this.model.locObj = locationList.filter(x => x.subLocations.length !== 0).map(x => { let childArray = Object.assign({}, x); return childArray.subLocations.filter(y => y._id == id).map(y => y)[0] })[0];
    }

    deleteLocation(id) {
        console.log("delete", id);
        const test = confirm("Are you sure, you want to delete Location");

        if (test) {
            this.myLocationService.deleteLocation(id)
                .subscribe(
                data => {
                    console.log("data", data);
                    this.model.locationList = data;
                },
                error => {
                    console.log(error);
                },
                () => {
                    //this.ngOnInit();
                }
                );
        }
    }
}
