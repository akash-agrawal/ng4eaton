import { Component, OnInit, Output } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router'

import { LoginService } from './login.service';
import { LoginModel } from './login';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    providers: [LoginService]
})
export class LoginComponent implements OnInit {
    private model: LoginModel;

    constructor(private loginService: LoginService, private router: Router) {
        this.model = new LoginModel();
    }

    ngOnInit(): void {
        if (localStorage.getItem('token')) {
            this.model.isLoggedIn = true;
        } else {
            this.model.isLoggedIn = false;
        }
    }

    loginUser() {
        this.model.loginSuccess = true;
        this.loginService.authenticateUser(this.model.useremail, this.model.password)
            .subscribe(
            data => {
                console.log("1", data);
                this.model.ro = data;
                localStorage.setItem('token', data.id_token);
            },
            error => {
                alert("Something went wrong!!");
            },
            () => {
                this.getTokenInfo(this.model.ro.id_token);
            }
            );
    }

    getTokenInfo(idToken) {
        this.loginService.tokenInfo(idToken)
            .subscribe(
            data => {
                console.log("2", data);
                this.model.tokenInfo = data;
                localStorage.setItem('profile', JSON.stringify(data));
            },
            error => {
                alert("Something went wrong!!");
            },
            () => {
                this.getAuth0(this.model.tokenInfo.identities[0].provider + '|' + this.model.tokenInfo.identities[0].user_id);
            }
            );
    }

    getAuth0(identity) {
        this.loginService.getAuth0(identity)
            .subscribe(
            data => {
                console.log("3", data);
                this.model.auth0 = data;
            },
            error => {
                alert("Something went wrong!!");
            },
            () => {
                this.model.loginSuccess = false;
                this.router.navigate(['/dashboard']);
            }
            );
    }
}
