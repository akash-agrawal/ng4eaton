export class LoginModel {
    public ro: any;
    public tokenInfo: any;
    public auth0: any;
    public loginSuccess: boolean;
    public useremail: string;
    public password: string;
    public isLoggedIn: boolean;

    constructor() {
        this.ro = [];
        this.tokenInfo = [];
        this.auth0 = [];
        this.loginSuccess = false;
        this.isLoggedIn = false;
    }
}
