import { Component, OnInit, ViewChild } from '@angular/core';
import { CreateAssetModalComponent } from '../common-services/modals/create-asset-modal/create-asset-modal.component';
import { ReplaceAssetModalComponent } from '../common-services/modals/replace-asset-modal/replace-asset-modal.component';
import { CloneAssetModalComponent } from '../common-services/modals/clone-asset-modal/clone-asset-modal.component';
import { NotificationComponent } from '../common-services/modals/notification/notification.component';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
    @ViewChild('createAssetModal') createAssetModal: CreateAssetModalComponent;
    @ViewChild('replaceAssetModal') replaceAssetModal: ReplaceAssetModalComponent;
    @ViewChild('cloneAssetModal') cloneAssetModal: CloneAssetModalComponent;

    constructor(public notify: NotificationComponent) { }

    ngOnInit() {
    }
}
