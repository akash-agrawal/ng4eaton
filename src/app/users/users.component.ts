import { Component, OnInit } from '@angular/core';

import { UsersService } from './users.service';
import { UsersModel } from './users';

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.css'],
    providers: [UsersService]
})
export class UsersComponent implements OnInit {
    //initializing model
    private model: UsersModel;

    //datatable column
    defaultFormscolumns = [
        { name: '' },
        { name: 'email' },
        { name: 'firstName' },
        { name: 'lastName' },
        { name: 'license' },
        { name: 'location' }
    ]

    constructor(private usersService: UsersService) {
        this.model = new UsersModel();
    }

    ngOnInit() {
        const tenantId = JSON.parse(localStorage.getItem('profile'));
        this.getUsers(tenantId.tenantId);
    }

    //calling service function to get all the users list
    getUsers(tenanatId) {
        this.usersService.getUsers(tenanatId)
            .subscribe(
            data => {
                console.log("data", data);
                this.model.usersList = data;
            },
            error => {
                console.log(error);
            });
    }


}
