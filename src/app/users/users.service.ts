import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../app.global';

@Injectable()
export class UsersService {

    constructor(private http: HttpClient) { }

    //service to get Users list
    getUsers(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'user/list/' + tenantId);
    }

}
