import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../app.global';

@Injectable()
export class FormFieldsService {

    constructor(private http: HttpClient) { }

    //service to get the form fields
    getFormFields(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'formField/list/' + tenantId);
    }

}
