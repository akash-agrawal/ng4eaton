import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { FormFieldsService } from './form-fields.service';
import { FormFieldsModel } from './form-fields';

@Component({
    selector: 'app-form-fields',
    templateUrl: './form-fields.component.html',
    styleUrls: ['./form-fields.component.css'],
    providers: [FormFieldsService]
})
export class FormFieldsComponent implements OnInit {
    //creating reference of the child component to the parent component
    @ViewChild('addFieldsModal') public addFieldsModal: ModalDirective;

    //initilializing model
    private model: FormFieldsModel;

    constructor(private formFieldsService: FormFieldsService) {
        this.model = new FormFieldsModel();
    }

        attcolumns = [
        { name: '' },
        { name: 'Field Name' },
        { name: 'Field Type' }
    ];

    //datatable column
    defaultFormscolumns = [
        { name: '' },
        { name: 'name' },
        { name: 'fieldType' }
    ]

    ngOnInit() {
        const tenantId = JSON.parse(localStorage.getItem('profile'));
        this.getFormFields(tenantId);
    }

    //calling service to get all form fields
    getFormFields(tenantId) {
        this.formFieldsService.getFormFields(tenantId)
            .subscribe(
            data => {
                console.log('Data', data);
                this.model.formFieldsList = data;
            },
            error => {
                console.log(error);
            },
            () => {
            });
    }

    //function to show "Add Field" modal
    addFields() {
        this.addFieldsModal.show();
    }

}
