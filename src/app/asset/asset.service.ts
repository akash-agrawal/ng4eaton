import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../app.global';

@Injectable()
export class AssetService {

    constructor(private http: HttpClient) { }

    //Original request before implementing Interceptor
    //getAllAssets(config) {
    //    const headers = new Headers();
    //    headers.append('Content-Type', 'application/json');
    //    headers.append('Authorization', "bearer " + localStorage.getItem('token'));
    //    const body = JSON.stringify(config);
    //    return this.http.post(myGlobals.baseUrl + 'asset/search', body);
    //    .map((response: Response) => response.json());
    //}

    getAllAssets(config) {
        const body = JSON.stringify(config);
        return this.http.post(myGlobals.baseUrl + 'asset/search', body);
    }

    search(value) {
        let config = {
            "assetNumber": value,
            "serialNumber": "",
            "description": "",
            "isLegacyCategory": null,
            "labelsCategories": null,
            "assetLocationIds": null,
            "ownerId": null,
            "chipId": "",
            "advanceSearch": true,
            "createdFromDate": "",
            "createdToDate": "",
            "expiryFromDate": "",
            "expiryToDate": "",
            "inServiceFromDate": "",
            "inServiceToDate": "",
            "outServiceFromDate": "",
            "outServiceToDate": "",
            "lastCompltedWorkFromDate": "",
            "lastCompltedWorkToDate": "",
            "nextFillDateFrom": "",
            "nextFillDateTo": "",
            "filterOn": true,
            "isQuickSearchOn": false,
            "pageNo": 1,
            "pageSize": 10,
            "order": "-createdDate",
            "listOn": false,
            "listOf": null,
            "initialLoad": true,
            "skipPaging": false
        };
        const body = JSON.stringify(config);
        return this.http.post(myGlobals.baseUrl + 'asset/search', body)
            .map((response: Response) => response.json());
    }
}
