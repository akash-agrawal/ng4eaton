export class AssetModel {
    public assetList: any;
    public assetCountedList: any;
    public totalRecord: number;
    public currentPage: number;
    public recordsPerPage: number;
    public totalPages: number;
    public showingCountTo: number;
    public showingCountFrom: number;
    public eatonContextMenu: Object;
    public contextMenuAsset: any;
    public config: any;
    public autoHeight: number;

    constructor() {
        this.assetList = [];
        this.assetCountedList = [];
        this.totalRecord = 0;
        this.currentPage = 0;
        this.recordsPerPage = 0;
        this.totalPages = 0;
        this.showingCountTo = 0;
        this.showingCountFrom = 0;
        this.eatonContextMenu = {};
        this.contextMenuAsset = [];
        this.config = [];
        this.autoHeight = 0;
    }
}
