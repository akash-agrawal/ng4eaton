import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../app.global';

@Injectable()
export class FormsService {

    constructor(private http: HttpClient) { }

    //service to get forms
    getForms(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'form/list/' + tenantId);
    }

    //service to get attachment labels
    getAttachmentLabels(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'attachmentLabel/list/' + tenantId + '?usedFor=FORM');
    }

}
