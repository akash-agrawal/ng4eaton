import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';

import { FormsService } from './forms.service';
import { FormsModel } from './forms';

@Component({
    selector: 'app-forms',
    templateUrl: './forms.component.html',
    styleUrls: ['./forms.component.css'],
    providers: [FormsService]
})
export class FormsComponent implements OnInit {
    //creating reference of the child component to the parent component
    @ViewChild('addFormDiagramModal') public addFormDiagramModal: ModalDirective;
    @ViewChild('addFormModal') public addFormModal: ModalDirective;

    //initializing model
    private model: FormsModel;

    constructor(private formsService: FormsService) {
        this.model = new FormsModel();
    }

    attcolumns = [
        { name: '' },
        { name: 'Name' },
        { name: 'Form Type' },
        { name: 'Title on Report' },
        { name: 'Action' }
    ];

    //datatable column
    defaultFormscolumns = [
        { name: '' },
        { name: 'name' },
        { name: 'formType' },
        { name: 'reportTitle' },
        { name: 'action' }
    ]

    ngOnInit() {
        const tenantId = JSON.parse(localStorage.getItem('profile'));
        this.getForms(tenantId.tenantId);
    }

    //calling service to get forms
    getForms(tenantId) {
        this.formsService.getForms(tenantId)
            .subscribe(
            data => {
                console.log('Data', data);
                this.model.formsList = data;
            },
            error => {
                console.log(error);
            },
            () => {
            });
    }

    //function to show "Add Diagram" modal and get attachment labels
    addDiagram() {
        this.addFormDiagramModal.show();
        const tenantId = JSON.parse(localStorage.getItem('profile'));
        this.getAttachmentLabels(tenantId.tenantId);
    }

    //calling service to get attachment labels
    getAttachmentLabels(tenantId) {
        console.log(tenantId);
        this.formsService.getAttachmentLabels(tenantId)
            .subscribe(
            data => {
                console.log('Data', data);
                this.model.attachmentLabels = data;
            },
            error => {
                console.log(error);
            },
            () => {
            });
    }

    //function to show "Add Form" modal
    addForm() {
        this.addFormModal.show();
    }
}
