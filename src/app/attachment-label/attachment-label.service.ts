import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';

import * as myGlobals from '../app.global';

@Injectable()
export class LabelsService {

    constructor(private http: HttpClient) { }

    //service to get all the attachment labels
    getLabels(tenantId) {
        return this.http.get(myGlobals.baseUrl + 'attachmentLabel/list/' + tenantId + '?usedFor=');
    }

}
