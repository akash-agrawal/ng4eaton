import { Component, OnInit } from '@angular/core';

import { LabelsService } from './attachment-label.service';
import { LabelsModel } from './attachment-label';

@Component({
    selector: 'app-attachment-label',
    templateUrl: './attachment-label.component.html',
    styleUrls: ['./attachment-label.component.css'],
    providers: [LabelsService]
})
export class AttachmentLabelComponent implements OnInit {
    //Initializing Model
    private model: LabelsModel;

    //datatables columns
    defaultFormscolumns = [
        { name: '' },
        { name: 'name' },
        { name: 'for' },
        { name: 'sync' }
    ]

    constructor(private labelsService: LabelsService) {
        this.model = new LabelsModel();
    }

    ngOnInit() {
        const tenantId = JSON.parse(localStorage.getItem('profile'));
        this.getLabels(tenantId.tenantId);
    }

    //calling service function to get all the attachment labels
    getLabels(tenanatId) {
        this.labelsService.getLabels(tenanatId)
            .subscribe(
            data => {
                this.model.labelsList = data;
            },
            error => {
                console.log(error);
            });
    }



}
