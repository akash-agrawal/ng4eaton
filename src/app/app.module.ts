import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ButtonsModule, PaginationModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { AlertModule, TooltipModule } from 'ngx-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';
import { ToastrModule } from 'toastr-ng2';
import { DragulaModule } from 'ng2-dragula';

import { routing } from './app.routing';
import { AuthGuard } from './common-services/authGuard/auth-guard.service';
import { TokenInterceptor } from './common-services/interceptor/token.interceptor';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AssetComponent } from './asset/asset.component';
import { AssetOverviewComponent } from './asset-overview/asset-overview.component';
import { HeaderComponent } from './layout/header/header.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { ErrorModalComponent } from './common-services/modals/error-modal/error-modal.component';
import { CreateAssetModalComponent } from './common-services/modals/create-asset-modal/create-asset-modal.component';
import { ReplaceAssetModalComponent } from './common-services/modals/replace-asset-modal/replace-asset-modal.component';
import { CloneAssetModalComponent } from './common-services/modals/clone-asset-modal/clone-asset-modal.component';
import { NotificationComponent } from './common-services/modals/notification/notification.component';
import { SetupComponent } from './setup/setup.component';
import { MyLocationComponent } from './my-location/my-location.component';
import { CategoryManagementComponent } from './category-management/category-management.component';
import { FormFieldsComponent } from './form-fields/form-fields.component';
import { FormsComponent } from './forms/forms.component';
import { UsersComponent } from './users/users.component';
import { AttachmentLabelComponent } from './attachment-label/attachment-label.component';
import { AddLocationModalComponent } from './common-services/modals/add-location-modal/add-location-modal.component';
import { ConfirmActionModalComponent } from './common-services/modals/confirm-action-modal/confirm-action-modal.component';
import { LayoutComponent } from './common-services/layout/layout.component';
import { LocationTreeViewComponent } from './common-services/modals/location-tree-view/location-tree-view.component';
import { AssetTemplateComponent } from './asset-template/asset-template.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        DashboardComponent,
        AssetComponent,
        AssetOverviewComponent,
        HeaderComponent,
        SidebarComponent,
        ErrorModalComponent,
        CreateAssetModalComponent,
        ReplaceAssetModalComponent,
        CloneAssetModalComponent,
        NotificationComponent,
        SetupComponent,
        MyLocationComponent,
        CategoryManagementComponent,
        FormFieldsComponent,
        FormsComponent,
        UsersComponent,
        AttachmentLabelComponent,
        AddLocationModalComponent,
        ConfirmActionModalComponent,
        LayoutComponent,
        LocationTreeViewComponent,
        AssetTemplateComponent
    ],
    imports: [
        routing,
        CommonModule,
        FormsModule,
        BrowserModule,
        HttpModule,
        HttpClientModule,
        ButtonsModule.forRoot(),
        PaginationModule.forRoot(),
        ModalModule.forRoot(),
        AlertModule.forRoot(),
        TooltipModule.forRoot(),
        NgxDatatableModule,
        BsDropdownModule.forRoot(),
        TabsModule.forRoot(),
        AccordionModule.forRoot(),
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        DragulaModule
    ],
    providers: [AuthGuard, NotificationComponent,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        }
    ],
    entryComponents: [ErrorModalComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
