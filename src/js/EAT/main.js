/**
 * @requires HBS
 * @requires jQuery
 */
(function() {
	/**
	 * @exports EAT.main
	 */
	var module = {};

	/**
	 * Global init code for the whole application
	 */
	module.init = function() {
		console.log('hello from main');
	};

	/**
	 * Initialize the app and run the bootstrapper
	 */
	$(document).ready(function() {
		module.init();
		HBS.initPage(true);
	});
	HBS.namespace('EAT.main', module);
}());
